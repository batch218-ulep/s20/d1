// console.log("Hello World!");

//[SECTION] While Loop
//"Iteration" is the term given to the repetition of statements
/*
- A while loop takes in an expression/condition
- Expressions are any unit of code that can evaluated to a value
- If the condition evaluates to true, the statements inside the block will be executed.
*/

//SYNTAX -> while(expression/condition){statement}

let count = 5;

while(count !== 4) {
	// 5 !== 4  -> T
	console.log("While: " + count);
	count--; 
	// (--) --> decrementation, decreasing the value by 1.
}

//[SECTION] Do-While Loop
//do{statement}
//while(expression/condition)
/*
let number = Number(prompt("Give me a number"));
	// "Number" function similar to the "parseInt" function.

do {
	// The current value of number is printed out
	console.log("Number: " + number);


	// Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	number += 1;
	//number = number + 1;
} while(number <= 10);
*/

//[SECTION] For Loops
/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

// (++) --> incrementation, adding 1 to a variable value.
for (let count = 10; count <= 20; count++){
	console.log(count);
}

/*for (let count = 0; count <= 20; count++){
	console.log(count);
}*/

let myString = "Alden";
console.log(myString.length);

console.log(myString[0]); //A
console.log(myString[1]); //l
console.log(myString[2]); //e

//console.log(myString[-1]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

// Create a string named "myName" with a value of your name

let myName = "CasSanDra";


for(let i = 0; i < myName.length; i++){
	//console.log(myName[i].toLowerCase());

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){
		// If the letter in the name is a vowel, it will print the number 3
		console.log("vowel found!")
		
	}else{
		console.log(myName[i]);
	}
}

//[SECTION] Continue and Break statements

for (let count = 0; count <= 20; count++){

	if (count % 2 === 0){
		// Tells the code to continue to the next iteration of the loop
		// This ignores all statements located after the continue statement;
		continue;
	}

	console.log("Continue and break: " + count);

	if (count > 10){
		// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
		// number values after 10 will no longer be printed
		break;
	}
}

